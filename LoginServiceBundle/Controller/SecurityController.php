<?php

namespace UDT\LoginServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use GuzzleHttp\Client as Client;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class SecurityController extends Controller
{

  private $username;
  private $sid;

  public function loginFormAction() {
    return $this->render('UDTLoginServiceBundle:Security:login.html.twig');
  }

  public function loginCheckAction() {

    $request  = Request::createFromGlobals();
    $username = $request->query->get('username');
    $sid      = $request->query->get('sid');

    $userProvider = $this->container->getParameter('udt.login_service.user_provider_bundle');
    $firewallName = $this->container->getParameter('udt.login_service.firewall_name');
    $securityName = "_security_$firewallName";

    if ($this->checkValidation($username, $sid) != true) {
      return new Response(
              'El Token de Ingreso no es válido, intente ingresar nuevamente', Response::HTTP_UNAUTHORIZED, array('Content-type' => 'application/json')
      );
    }

    $em   = $this->getDoctrine()->getManager();
    $user = $em->getRepository($userProvider)->loadUserByUsername($username);

    if (!$user) {
      return new Response(
              'El Usuario no tiene permisos para ingresar a este módulo', Response::HTTP_UNAUTHORIZED, array('Content-type' => 'application/json')
      );
    }

    $token = new UsernamePasswordToken($user, null, $firewallName, $user->getRoles());
    $this->get('security.token_storage')->setToken($token);
    $this->get('session')->set($securityName, serialize($token));


    // Fire the login event manually
    $event = new InteractiveLoginEvent($request, $token);
    $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

    return new Response(
            'Ingreso Exitoso', Response::HTTP_OK, array('Content-type' => 'application/json')
    );
  }

  private function checkValidation($username, $sid) {
    $linkVerificador = "http://media.fen.uchile.cl/logmod/verificacion.php?username=$username&sid=$sid";
    $client          = new Client();
    $res             = $client->request('GET', $linkVerificador);
    $token           = $res->getBody()->getContents();

    return $token;
  }

}
