<?php

namespace UDT\LoginServiceBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class UDTLoginServiceExtension extends Extension
{

  const LOGIN_SERVICE_DEFINITION = 'udt.login_service';

  public $box;
  public $config;

  /**
   * {@inheritdoc}
   */
  public function load(array $configs, ContainerBuilder $container) {
    $configuration = new Configuration();
    $this->config  = $this->processConfiguration($configuration, $configs);
    $this->box     = $container;

    // Constructor del servicio de Login
    $this->setLoginServiceDefinition();

    $loader = new Loader\YamlFileLoader($this->box, new FileLocator(__DIR__ . '/../Resources/config'));
    $loader->load('services.yml');
  }

  private function setLoginServiceDefinition() {
    foreach ($this->config as $key => $value) {      
      $name = self::LOGIN_SERVICE_DEFINITION.'.'.$key;
      $this->box->setParameter($name, $value);     
    }
  }

}
