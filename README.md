# UDT Plugins
## LoginService

Este Plugin integra el actual LOGMOD con los servicios de Symfony3 para poder realizar
un login exitoso.

###Agregar a Composer (en 'require')
```javascript
  "require": {
    ...
    ...
    "udt/login_service_bundle": "dev-master"   
  },
  "repositories": [{
      "type": "vcs",
      "url": "https://bitbucket.org/hbozan/udt.loginservice.git"
  }],
```

###Agregar en AppKernel

```php
  new UDT\LoginServiceBundle\UDTLoginServiceBundle()
```

`
###Configuración
Configuration of config.yml
```yml
udt_login_service:
  form_location: 'UDTLoginServiceBundle:Security:login.html.twig'
  user_provider_bundle: 'UDTAppPackageUsuarioBundle:Usuario'
  firewall_name: 'main'
```

donde 
* 'udt_login_service': Corresponde al nombre de la bundle
* 'form_location': Corresponde a la ubicación del formulario de login y donde estará el IFRAME de LOGMOD
* 'user_provider_bundle': Corresponde a saber cuál será el proveedor de Usuarios
* 'firewall_name': Nombre del Firewall dentro del service que se usará